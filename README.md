# Short and Simple Face Detection:

## Where did this come from?
This originally came from a tutorial video so big shout out to the creator. Of course I had to read up on the [tensorflow](https://justadudewhohacks.github.io/face-api.js/docs/globals.html) documentation for JS to understand what each method and function calls were doing. There is plenty to learn just with JS as the video has shown new methods of implementations.

## What does it do?
With the prerequisite to allow/enable camera access, once program is running, the browser will use your face-cam to record your face and attempt to detect your facial expression. 

## The Hows
Now just watching the video is no good. Just watching any tutorial is not very useful so of course I wrote plenty of notes for myself and what the code does. 

**The canvas**
the div tag here is to keep the canvas of where the camera recording will show. This is attempts to prevent variable screensize which would have made the detection border difficult to implement.
```html=14
    <div class="flex-container">
        <div class="layer1">
            <header>Face Detection with JS</header>
        </div>
        <div class="player">
            <video id="video" width="720" height="560" autoplay muted></video>
        </div>
    </div>
```

**JS implementation**
```javascript=
// get video element
const video = document.getElementById('video')

// need to load all async call in parallel to detect face
// this is all done async so you need to use promise.all for quick execution
Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
    faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
    faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
    faceapi.nets.faceExpressionNet.loadFromUri('/models')
]).then(startVideo)
// faceapi.nets.(whatever model you want).loadFromUri('(location of where all the models are')

// this function to hook up our webcam to video element
function startVideo() {
    // navigator.getusermedia{ object where video is key with empty object as parameter}
    // stream is what is coming from the webcam (but what is video.srcObject ?)
    navigator.getUserMedia(
        { video: {} },
        stream => video.srcObject = stream,
        err => console.error(err)
    )
}
```

## Challenges
Using the `faceLandmark68Net` method, there seems to be some alignment or calibration issues as it does not find the correct landmarks of the face. Since I do not fully understand given the documentation, there could be a possibility that it is due to the given models, resolution, or just my incompetence.

No other particular challenges in terms of coding since much of it is from the tutorial but rather better coding practices.

## Potential additions/changes/features
* try to do the same but with prerendered video (not webcam)
* try with images and slide shows
* try with detecting what is on screen instead of video
* try age estimation and gender recognition

