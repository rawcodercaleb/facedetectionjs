// get video element
const video = document.getElementById('video')

// need to load all async call in parallel to detect face
// this is all done async so you need to use promise.all for quick execution
Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
    faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
    faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
    faceapi.nets.faceExpressionNet.loadFromUri('/models')
]).then(startVideo)
// faceapi.nets.(whatever model you want).loadFromUri('(location of where all the models are')

// this function to hook up our webcam to video element
function startVideo() {
    // navigator.getusermedia{ object where video is key with empty object as parameter}
    // stream is what is coming from the webcam (but what is video.srcObject ?)
    navigator.getUserMedia(
        { video: {} },
        stream => video.srcObject = stream,
        err => console.error(err)
    )
}


// when video starts playing then do the code as written:
video.addEventListener('playing', () => {
    // console.log('asdfasdf')
    // do the face detection part:
    const canvas = faceapi.createCanvasFromMedia(video)
    document.body.append(canvas)
    const displaySize = { width: video.width, height: video.height }
    faceapi.matchDimensions(canvas, displaySize)
    setInterval(async () => {
        const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
        // console.log(detections)
        const resizeDetections = faceapi.resizeResults(detections, displaySize)
        canvas.getContext('2d').clearRect(0,0, canvas.width, canvas.height)
        faceapi.draw.drawDetections(canvas, resizeDetections)
        faceapi.draw.drawFaceLandmarks(canvas, resizeDetections)
        faceapi.draw.drawFaceExpressions(canvas, resizeDetections)
    }, 100)
    // in the console you will see details of what is detected. you want to display those informations
})

// the 'straight-forward' stuff
// setInveral to run code inside of this multiple times
// async function since async library
// get detections is where all the face inside of the webcam image everytime this code is called every 100ms
// we are using tinyfaceapi (TinyFaceDetectorOptions) and detect with facelandmarks, expressions 
// note the canvas element which we created in this file, must be stationary for face api to detect face within the canvas
// the displaysize is for perfect over canvas to keep consistent size
// use detections variable and resizeddetections to get proper sized box around face for the video element and canvas
// the draw displays it on canvas
// after we resize everything, we want canvas get context 2d since 2d object and clear it
// faceapi.matchdimension is to keep same size of canvas and displaysize
// face.draw etc
